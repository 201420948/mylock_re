#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <assert.h>
#include <sys/queue.h>
#include <unistd.h>
#include "config.h"
#include "locks.h"

static int nr_slots = 0;

static enum lock_types lock_type;
int values[DEFAULT_NR_RINGBUFFER_SLOTS];
static int in=0;
static int out=0;
struct spinlock lock;
void (*enqueue_fn)(int value) = NULL;
int (*dequeue_fn)(void) = NULL;

void enqueue_ringbuffer(int value)
{
    assert(enqueue_fn);
    assert(value >= MIN_VALUE && value < MAX_VALUE);

    enqueue_fn(value);
}

int dequeue_ringbuffer(void)
{
    int value;

    assert(dequeue_fn);

    value = dequeue_fn();
    assert(value >= MIN_VALUE && value < MAX_VALUE);

    return value;
}


/*********************************************************************
 * TODO: Implement using spinlock
 */
void enqueue_using_spinlock(int value)
{
    
    again:
        acquire_spinlock(&lock);
        if((in+1)%16==out){ 
            release_spinlock(&lock);
            usleep(10);
            goto again;
        }
    
    values[in]=value;
    in = (in+1)%16;
    release_spinlock(&lock);
}

int dequeue_using_spinlock(void)
{
    again:
        acquire_spinlock(&lock);
        if(in == out){ 
            release_spinlock(&lock);
            usleep(10);
            goto again;
        }
    int value;
    value=values[out];
    out = (out+1)%16;
    release_spinlock(&lock);

    return value;
}

void init_using_spinlock(void)
{
    
    enqueue_fn = &enqueue_using_spinlock;
    dequeue_fn = &dequeue_using_spinlock;
}

void fini_using_spinlock(void)
{
}


/*********************************************************************
 * TODO: Implement using mutex
 */
void enqueue_using_mutex(int value)
{

}

int dequeue_using_mutex(void)
{
    return 0;
}

void init_using_mutex(void)
{
    enqueue_fn = &enqueue_using_mutex;
    dequeue_fn = &dequeue_using_mutex;
    
}

void fini_using_mutex(void)
{
}


/*********************************************************************
 * TODO: Implement using semaphore
 */
void enqueue_using_semaphore(int value)
{
}

int dequeue_using_semaphore(void)
{
    return 0;
}

void init_using_semaphore(void)
{
    enqueue_fn = &enqueue_using_semaphore;
    dequeue_fn = &dequeue_using_semaphore;
}

void fini_using_semaphore(void)
{
}


/*********************************************************************
 * Common implementation
 */
int init_ringbuffer(const int _nr_slots_, const enum lock_types _lock_type_)
{
    assert(_nr_slots_ > 0);
    nr_slots = _nr_slots_;

    /* Initialize lock! */
    lock_type = _lock_type_;
    switch (lock_type) {
    case lock_spinlock:
        init_using_spinlock();
        break;
    case lock_mutex:
        init_using_mutex();
        break;
    case lock_semaphore:
        init_using_semaphore();
        break;
    }

    /* TODO: Initialize your ringbuffer and synchronization mechanism */

    return 0;
}

void fini_ringbuffer(void)
{
    /* TODO: Clean up what you allocated */
    switch (lock_type) {
        case lock_spinlock:
        fini_using_mutex();
        break;
    case lock_mutex:
        fini_using_mutex();
        break;
    case lock_semaphore:
        fini_using_semaphore();
        break;
    }
}